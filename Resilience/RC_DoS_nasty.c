/* Robust Combiner - work in progress
 * to be executed on h3, when running IsolatedCentralRC3SlavesTopo.py
 * Author: Philipp Heyder
 *	27.8.
 *	Timestamps now use clock_gettime 
 */
#include <unistd.h>		// to get PID

#include<netinet/in.h>
#include<errno.h>
#include<netdb.h>
#include<stdio.h>
#include<stdlib.h>    		// malloc() and free()
#include<string.h>    		// memcmp
#include<netinet/ip.h>    	// declarations for ip header
#include<netinet/if_ether.h>  	// ETH_P_ALL
#include<net/ethernet.h>  	// ether_header
#include<sys/socket.h>
#include<sys/types.h>
#include<linux/if_packet.h>	// for socket_address.sll_pkttype
#include<signal.h>
#include<pthread.h>
#include<sys/ioctl.h>		// for find_ifIndex
#include<net/if.h>		// for find_ifIndex, supplies struct ifr
#include<time.h>		// timestamping datatypes

#define MAX_PKT_LENGTH 1600	// upper bound for packet size, standard MTU for Ethernet: 1512
#define PACKET_CACHE 100	// upper bound for number of packets cached in RC
#define MAJORITY 2		// number of votes needed for majority descission
//#define uint64_t TIMEOUT	PACKET_CACHE*100000000	// nsec
//100000000 works ok (for ping?)

//DoS:
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define CRIT_DISTANCE 30
#define BLOCK_LIMIT 5

/*Globals*******************************************************************************/
int loop = 1;
int port_blocked = 0;	// while one port is blocked, others are prevented from being blocked as well
unsigned long long TIMEOUT = 100000000;	//100ms
int sock_raw_0, sock_raw_1, sock_raw_2, sock_raw_3, sock_raw_4, sock_raw_5, sock_raw_6, sock_raw_7;
unsigned char empty[MAX_PKT_LENGTH];			// 0 string, to check if cache field is empty

static pthread_mutex_t printf_mutex;			// create mutex lock for printfs
static pthread_mutex_t cache0_mutex;			// create mutex locks for writing and searching for each cache
static pthread_mutex_t cache1_mutex;
static pthread_mutex_t cache2_mutex;
static pthread_mutex_t cache3_mutex;
static pthread_mutex_t cache4_mutex;
static pthread_mutex_t cache5_mutex;

// socket setup recvfrom()
struct sockaddr saddr;					// needed to declare sendto
int saddr_size = sizeof(saddr);

struct pkt_struct{					// packet from buffer gets moved here + seperated metadata
  unsigned char full_packet[MAX_PKT_LENGTH];
  int data_size;
  int timestamp;
};

//     thread input struct
struct recv_thread_input{
  int sock_id;
  int head;			// offset to next empty element
  int tail;			// offset to next element to move to candidate_queue
  int distance;
  int block;
  char *port;
  struct pkt_struct * cache_start;
  pthread_mutex_t cache_mutex;
};

struct candidate{
  unsigned char full_packet[MAX_PKT_LENGTH];
  unsigned char port1;
  unsigned char port2;
  unsigned char port3;
  int data_size;
  struct timespec timestamp;
};

//     thread input struct
struct collect_thread_input{
  char left;				// TODO: can be replaced as well
  int last_candidate;			// offset to first empty element - same as head in struct recv_thread_input
  struct candidate * candidate_queue;
  struct recv_thread_input *cache1;
  struct recv_thread_input *cache2;
  struct recv_thread_input *cache3;
  struct sockaddr_ll socket_address;
  int send_sock_id;
};

/*Prototypes****************************************************************************/
void sig_handler(int sig_num);
// int read_from_buffer(struct recv_thread_input * cache, struct candidate * candidate_queue, int left, unsigned char * temp_Packet, int which_cache, int send_sock_id, struct sockaddr_ll socket_address, int * last);
// int write_to_portbuffer(pkt_struct * cache);		// returns index of element, that was written to
// TODO complete when function parameters are final

/*Function Declarations*****************************************************************/
// falsifies loop condition on keyboard interrupt, TODO unblocking recvfrom() doesn't work
void sig_handler(int sig_num){
  if(sig_num == SIGINT){
    printf("\n Caught the SIGINT signal\n Stopping and cleaning up...\n");
    shutdown(sock_raw_0, SHUT_RD);
    shutdown(sock_raw_1, SHUT_RD);
    shutdown(sock_raw_2, SHUT_RD);
    shutdown(sock_raw_3, SHUT_RD);
    shutdown(sock_raw_4, SHUT_RD);
    shutdown(sock_raw_5, SHUT_RD);
    loop = 0;
  }else{
    printf("\n Caught the signal number [%d]\n", sig_num);
  }
}

// returns the interface index, input is interface name 
int find_ifIndex(char * if_name){
  
  struct ifreq ifr;
  int fd = socket(AF_PACKET,SOCK_RAW,0);
  if (fd==-1) {
    printf("Socket creation fail %s",strerror(errno));
  }
  size_t if_name_len=strlen(if_name);
  // char if_name[] = "h3-eth5";

  if (if_name_len<sizeof(ifr.ifr_name)) {
    memcpy(ifr.ifr_name,if_name,if_name_len);
    ifr.ifr_name[if_name_len]=0;
//     printf("if_name via struct= %s\n",ifr.ifr_name);
  } else {
    printf("interface name is too long");
  }

  if (ioctl(fd,SIOCGIFINDEX,&ifr)==-1) {
    printf("Error in ioctl %s",strerror(errno));
  }
//   printf("ifr_index = %d\n", ifr.ifr_ifindex);
  close(fd);
  return ifr.ifr_ifindex;
}

// packet reception and handling ring buffer
void *receive_setup(void *threadarg){
  
  struct recv_thread_input *my_data = (struct recv_thread_input *) threadarg;
  printf("\n---%s with Socket_ID: %d runs in Thread %lu cache addr: %p\n", 
	 my_data->port, my_data->sock_id, pthread_self(), my_data->cache_start);
  int i;
  my_data->head = 0;
  my_data->tail = 0;
  my_data->distance = 0;
  my_data->block = 0;
  
  do{
    my_data->cache_start[my_data->head].data_size = 
      recvfrom(my_data->sock_id, my_data->cache_start[my_data->head].full_packet,
      MAX_PKT_LENGTH , 0 , &saddr , (socklen_t*)&saddr_size);
    if(my_data->cache_start[my_data->head].full_packet[0] == 0){
      /*pthread_mutex_lock(&printf_mutex);
	printf("\n%s ", my_data->port);
	for(i=0;i<20;i++){printf("%d ", my_data->cache_start[my_data->head].full_packet[i]);}
	printf("\n");
      pthread_mutex_unlock(&printf_mutex);
      */
      my_data->cache_start[my_data->head].full_packet[7] = 200;					// adjust TOPOLOGY so this isn't needed anymore
      pthread_mutex_lock(&my_data->cache_mutex);
      my_data->head++;
      pthread_mutex_unlock(&my_data->cache_mutex);
//       if(my_data->head >= PACKET_CACHE + 1){
      if(my_data->head >= PACKET_CACHE - 1){
	/*pthread_mutex_lock(&printf_mutex);
	printf("\nEnd of cache %s--------------------------------------------------------+++\n", my_data->port);
	pthread_mutex_unlock(&printf_mutex);
	*/
	while(my_data->tail <= (my_data->head - 1) ){};
	pthread_mutex_lock(&my_data->cache_mutex);
	my_data->head = 0;
	my_data->tail = 0;
	pthread_mutex_unlock(&my_data->cache_mutex);
	/*pthread_mutex_lock(&printf_mutex);
	printf("\nWaking back up %s----head: %d---tail: %d-------------------------------+++\n", my_data->port, my_data->head, my_data->tail);
	pthread_mutex_unlock(&printf_mutex);
	*/
      }
      
    }else{	// packet from outside mininet
//       printf("\n.");
    }
    // block if spammed consistently
    if(my_data->block > 4 && port_blocked == 0){
      printf("%s blocked!***************\n", my_data->port);
      port_blocked = 1;
      my_data->head = 0;
      my_data->tail = 0;
      sleep(5);
      my_data->block = 0;
      port_blocked = 0;
      printf("%s unblocked**************\n", my_data->port);
    }
    
  }while(loop);

  return 0;
}

// cycles through reception caches, starts search for each
void *collect_and_send(void *threadarg){
  
  struct collect_thread_input *my_data = (struct collect_thread_input *) threadarg;
  int min, max;	// kann weg?
  //unsigned char * temp_Packet = (unsigned char *) malloc(sizeof(unsigned char) * MAX_PKT_LENGTH);
  int pref_block;
  unsigned char * temp_Packet[MAX_PKT_LENGTH];
  my_data->last_candidate = 0;
  pthread_mutex_lock(&printf_mutex);
  if(my_data->left == 0){printf("\n***Right queue");}else{printf("***Left queue");}
    printf("\n***Collect Thread %lu watches: \n C1 %p\n C2 %p\n C3 %p", pthread_self(), my_data->cache1->cache_start, my_data->cache2->cache_start, my_data->cache3->cache_start);
    printf("\nwrites to: %p\n", my_data->candidate_queue);
  pthread_mutex_unlock(&printf_mutex);
  //printf("Last: %p\n", &(my_data->last_candidate));

  while(loop){
/*
    // just to test that all is well:
    if(pref_block < 0 || pref_block > 3){printf("\n-----------------------PREF_BLOCK IS FUCKED--------------\n"); sleep(5);}
    // this may trigger due to threading recieve is halted, but reading continues
    if(my_data->cache1->block < 0 || my_data->cache1->block > 5){printf("\n---------block cache1 IS FUCKED: %d--------------\n",my_data->cache1->block) ; sleep(5);
      
    }
    if(my_data->cache2->block < 0 || my_data->cache2->block > 5){printf("\n---------block cache2 IS FUCKED--------------\n"); sleep(5);}
    if(my_data->cache3->block < 0 || my_data->cache3->block > 5){printf("\n---------block cache3 IS FUCKED--------------\n"); sleep(5);}
    //tests done
    */
    my_data->cache1->distance = 0;
    my_data->cache2->distance = 0;
    my_data->cache3->distance = 0;
    
    if(my_data->cache1->block < 5){
    read_from_buffer(my_data->cache1, my_data->candidate_queue, my_data->left, temp_Packet, 1, my_data->send_sock_id, my_data->socket_address, &(my_data->last_candidate));
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
    }
    if(my_data->cache2->block < 5){
    read_from_buffer(my_data->cache2, my_data->candidate_queue, my_data->left, temp_Packet, 2, my_data->send_sock_id, my_data->socket_address, &(my_data->last_candidate));
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
    }
    if(my_data->cache3->block < 5){
    read_from_buffer(my_data->cache3, my_data->candidate_queue, my_data->left, temp_Packet, 3, my_data->send_sock_id, my_data->socket_address, &(my_data->last_candidate));
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
    }
    max = max(max(my_data->cache1->distance, my_data->cache2->distance), my_data->cache3->distance);
    min = min(min(my_data->cache1->distance, my_data->cache2->distance), my_data->cache3->distance);
    
    if(max - min > CRIT_DISTANCE){
      pthread_mutex_lock(&printf_mutex);
//       printf("----Distances---------------------\n");
//       printf("Cache1: %d\n", my_data->cache1->distance);
//       printf("Cache2: %d\n", my_data->cache2->distance);
//       printf("Cache3: %d\n", my_data->cache3->distance);

      if (my_data->cache1->distance > my_data->cache2->distance && my_data->cache1->distance > my_data->cache3->distance && port_blocked == 0){
	my_data->cache1->block++;
	printf("Cache1 block++\n");
	if(pref_block != 1){
	  if(my_data->cache2->block > 0 && my_data->cache2->block < BLOCK_LIMIT){
	    my_data->cache2->block--;
	  }
	  if(my_data->cache3->block > 0 && my_data->cache3->block < BLOCK_LIMIT){
	    my_data->cache3->block--;
	  }
	}
	pref_block = 1;
      }
      if (my_data->cache2->distance > my_data->cache1->distance && my_data->cache2->distance > my_data->cache3->distance && port_blocked == 0){
	my_data->cache2->block++;
	printf("Cache2 block++\n");
	if(pref_block != 2){
	  if(my_data->cache1->block > 0 && my_data->cache1->block < BLOCK_LIMIT){
	    my_data->cache1->block--;
	  }
	  if(my_data->cache3->block > 0 && my_data->cache3->block < BLOCK_LIMIT){
	    my_data->cache3->block--;
	  }
	}
	pref_block = 2;
      }
      if (my_data->cache3->distance > my_data->cache1->distance && my_data->cache3->distance > my_data->cache2->distance && port_blocked == 0){
	my_data->cache3->block++;
	printf("Cache3 block++\n");
	if(pref_block != 3){
	  if(my_data->cache1->block > 0 && my_data->cache1->block < BLOCK_LIMIT){
	    my_data->cache1->block--;
	  }
	  if(my_data->cache2->block > 0 && my_data->cache2->block < BLOCK_LIMIT){
	    my_data->cache2->block--;
	  }
	}
	pref_block = 3;
      }
      pthread_mutex_unlock(&printf_mutex);
    }
  }
  return 0;
}

// compares each packet from cache to each packet from candidate_queue
int read_from_buffer(struct recv_thread_input * cache, struct candidate * candidate_queue, int left, unsigned char * temp_Packet, int which_cache, int send_sock_id, struct sockaddr_ll socket_address, int * last){
  
  int i, j, a;
  int data_size = 0;
  int amount_send;
  int result;
  unsigned long long duration;
  struct timespec end;	// timing
  
  struct candidate clear_candidate;
    memset(clear_candidate.full_packet, 0, MAX_PKT_LENGTH); 
    clear_candidate.port1 = 0;
    clear_candidate.port2 = 0;
    clear_candidate.port3 = 0;
    clear_candidate.data_size = 0;
    clear_candidate.timestamp.tv_sec = 0;
    clear_candidate.timestamp.tv_nsec = 0;

  cache->distance = cache->head - cache->tail;
  // combiner start:
  if(cache->head < cache->tail){
//     printf("\n***tail overtook head: something is off***\nhead: %d tail %d\n", cache->head, cache->tail); 
    cache->tail = 0;
    
  }
  while(cache->head > cache->tail && loop){  				// as long as there are elements in the queue
    
    memset(temp_Packet, 0, MAX_PKT_LENGTH);				// TODO this can be improved: we don't need a temp packet anymore
    data_size = cache->cache_start[cache->tail].data_size;
    memcpy(temp_Packet, cache->cache_start[cache->tail].full_packet, cache->cache_start[cache->tail].data_size); // pick tail element for insertion into candidate_queue
/*
    if(left == 0){printf("\n\t\t\t\t\t\t\t\tRight");}else{printf("\nLeft");}
    printf("\n\tStart of while loop, Cache%d head: %d, tail: %d PKT ID: %d", which_cache, cache->head, cache->tail, temp_Packet[19]);
//     temp_Packet[19] is first byte of IP Identification field, serves to superficially identify the packet
*/

    for(j=0; (j<=*last && loop); j++){						// loop over candidate_queue
       if(*last==PACKET_CACHE-1){						// if candidate_queue is full
// 	  if(left == 0){printf("\t\t\t\tRight");}else{printf("Left");}			// -> start crude clean up
// 	  printf("Queue almost full!!\n");
	  /*pthread_mutex_lock(&printf_mutex);
	  printf("Before cleaning j = %d, last = %d\n",j , *last);
	  for(a=0;a<=PACKET_CACHE-1;a++){
	    printf("[%d] ", a);
	    for(b=0;b<20;b++){printf("%d ", candidate_queue[a].full_packet[b]);}
	    printf("   |   p1: %d p2: %d p3: %d", candidate_queue[a].port1, candidate_queue[a].port2, candidate_queue[a].port3);
	    printf("   |   %ld clks\n", clock() - candidate_queue[a].timestamp);
	  }
	  pthread_mutex_unlock(&printf_mutex);
	  */
 	  j=0;
	  while(j<*last){
// 	    printf("Vorher: %d\n", a);
	    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	    duration = (end.tv_nsec - candidate_queue[j].timestamp.tv_nsec);
	    if(duration>TIMEOUT){
// 	      printf("CACHE FULL: clean [%d], a = %d, last = %d, dur: %llu\n", candidate_queue[j].full_packet[19], j, *last, duration);
	      result = 3;
	      clean_Candidate(candidate_queue, j, last, left, data_size, result);
	    }else{
// 	      printf("Don't have to clean: a = %d, last = %d, clk= %ld\n", a, *last, (clock() - candidate_queue[a].timestamp));
	      
	    }
	    j++;
// 	    printf("Nachher: %d\n", a);
	  }
// 	  printf("Ended Cache_cleanup with a = %d and last = %d", a, *last);
	  j=0;
	  
	 /*
	 for(i=0;i<PACKET_CACHE-1;i++){
	   candidate_queue[i] = clear_candidate;
	   if(i<20){
	     candidate_queue[i]=candidate_queue[PACKET_CACHE-i];
	  }
	}
	*/
      }
 	   
      if(j==*last){			// if candidate element is empty
// 	printf("\nFound empty field, Writing to candidate_queue[%d]", j);
// 	pthread_mutex_lock(&printf_mutex);
// 	if(left == 0){printf("\n\t\t\t|\t");}else{printf("\n");}
// 	printf("Write ID %d to [%d]", temp_Packet[19], j);
// 	if(left != 0){printf("\t|");}
// 	pthread_mutex_unlock(&printf_mutex);
	
	    switch(which_cache){							// and adjust port counter
	      case 1:	candidate_queue[j].port1++;
			if(candidate_queue[j].port1>1){printf("\n\n!!Empty field has flooded ports: Port%d!!\n\n", which_cache);
			  sleep(2);
			  return 0;}
			break;
	      case 2:	candidate_queue[j].port2++;
			if(candidate_queue[j].port2>1){printf("\n\n!!Empty field has flooded ports: Port%d!!\n\n", which_cache);
			  sleep(2);
			  return 0;}
			break;
	      case 3:	candidate_queue[j].port3++;
			if(candidate_queue[j].port3>1){printf("\n\n!!Empty field has flooded ports: Port%d!!\n\n", which_cache);
			  sleep(2);
			  return 0;}
			break;
	      default:	printf("\n\n!!Trouble with read_from_buffer() input!!\n\n");
	    }
	memcpy(candidate_queue[j].full_packet, temp_Packet, data_size);
// 	candidate_queue[j].timestamp = clock();
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &candidate_queue[j].timestamp);
	*last = *last + 1;	//works
//	printf("\nAfter increase last value: %d, address: %p", *last, last);
	pthread_mutex_lock(&cache->cache_mutex);
	cache->tail++;
	pthread_mutex_unlock(&cache->cache_mutex);
	if(cache->tail == cache->head){						// if end of buffer reached
	  return 0;									// -> end search 
	}										// TODO but this should be already taken care of by while condition
	break;
      }
	  
      if(!memcmp(temp_Packet, candidate_queue[j].full_packet, data_size)){		// if temp_packet matches candidate
//  	    pthread_mutex_lock(&printf_mutex);
	    switch(which_cache){							// -> update port counter
	      case 1:	candidate_queue[j].port1++;					
			if(candidate_queue[j].port1>1 && cache->tail != 0 ){
			  //printf("\n\n!!Packet was already received at this Port%d!!\n\n", which_cache);
			  //wait(2);
			  break;}
			break;
	      case 2:	candidate_queue[j].port2++;
			if(candidate_queue[j].port2>1 && cache->tail != 0){printf("\n\n!!Packet was already received at this Port%d!!\n\n", which_cache);
			  wait(2);
			  return 0;}
			break;
	      case 3:	candidate_queue[j].port3++;
			if(candidate_queue[j].port3>1 && cache->tail != 0){printf("\n\n!!Packet was already received at this Port%d!!\n\n", which_cache);
			  wait(2);
			  return 0;}
			break;
	      default:	printf("\n\n!!Trouble with comb_buffer() input!!\n\n");
	    }
	    if(cache->head == cache->tail){						// if end of reveive cache reached
// 	      printf("\nSearch through Cache%d complete\n", which_cache);
	      return 0;									// -> return from search, receive thread will handle clean up
	    }else{
	      pthread_mutex_lock(&cache->cache_mutex);
	      cache->tail++;
	      pthread_mutex_unlock(&cache->cache_mutex);
	    }
	      
	    if(candidate_queue[j].port1 + candidate_queue[j].port2 + candidate_queue[j].port3 == MAJORITY){
	      amount_send = sendto(send_sock_id, temp_Packet, data_size, 0, (struct sockaddr*)&socket_address, sizeof(socket_address));
	      if (amount_send<0){
		perror("\nsendto failed");
	      }else{
// 		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
// 		printf("ID %d send after %lld sec, %lld nsec\n", temp_Packet[19], (long long unsigned int)(end.tv_sec - candidate_queue[j].timestamp.tv_sec),(long long unsigned int)(end.tv_nsec - candidate_queue[j].timestamp.tv_nsec));
		/*if((end.tv_nsec - candidate_queue[j].timestamp.tv_nsec)>10){
		  printf("\nBusted\n");
		}*/
// 	      	pthread_mutex_lock(&printf_mutex);
// 		if(left == 0){printf("\n\t\t\t|\t");}else{printf("\n");}
// 		printf("Send ID %d, from [%d] after %ld clks\n", temp_Packet[19], j, (clock() - candidate_queue[j].timestamp));
// 		if(left != 0){printf("\t|");}
// 		pthread_mutex_unlock(&printf_mutex);
	      } 
	    }else if(candidate_queue[j].port1 + candidate_queue[j].port2 + candidate_queue[j].port3 > MAJORITY){
// 	      printf("CORRECT: Clean ID %d after 3rd pkt arrived\n", temp_Packet[19]);
	      result = 0;
	      clean_Candidate(candidate_queue, j, last, left, data_size, result);
	      j--;
// 	      printf("\n--------------After clean jump\n");
	    }
	    break;
	}
	/*
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	if((end.tv_nsec - candidate_queue[j].timestamp.tv_nsec) > TIMEOUT){
	  printf("TIMEOUT: Clean ID %d [%d], after timeout: %llu nsec -> cleanup\n", temp_Packet[19], j,(unsigned long long) (end.tv_nsec - candidate_queue[j].timestamp.tv_nsec));
	  result = 1;
	  clean_Candidate(candidate_queue, j, last, left, data_size, result);
	  j--;
	}*/
      }
  }
  return 0;
}

// called to remove single packet from candidate_queue
int clean_Candidate(struct candidate * queue, int j, int * last, int left, int data_size, int reason){
//   printf("\ncleaned %d", queue[j].full_packet[19]);
//   printf("\nEntered clean_Candidate\n");
  int i, x;
  /*pthread_mutex_lock(&printf_mutex);
  printf("\n");
  if(left){printf("LEFT\n");}else{printf("\t\t\tRIGHT\n");}
  if(reason == 0){
    printf("----CORRECT Clean ID %d, j = %d, last value: %d, address: %p\n", queue[j].full_packet[19], j, *last, last);
  }
  if(reason == 1){
    printf("----TIMEOUT Clean ID %d last value: %d, after %ld clk\n", queue[j].full_packet[19], *last, clock() - queue[j].timestamp);
  }
    if(reason == 3){
    printf("----Cache Clean ID %d last value: %d, after %ld clk\n", queue[j].full_packet[19], *last, clock() - queue[j].timestamp);
  }/*
  printf("Before cleaning:\n");
  for(i=0;i<PACKET_CACHE;i++){
    if(i==j){printf("X");}else{printf(" ");}
    printf("[%d]", i);
    for(x=0;x<20;x++){printf("%d ", queue[i].full_packet[x]);}
    printf("   |   p1: %d p2: %d p3: %d\n", queue[i].port1, queue[i].port2, queue[i].port3);
  }
  
  pthread_mutex_unlock(&printf_mutex);
  */
  if((*last==0) && reason !=3){			// clean_Candidate_cache may clean all elements, otherwise clean_Candidate will only be called after packet arrived
    printf("\n\n!!!Error *last == 0 and clean_Candidate was called!!\n\n");
    sleep(5);
    return 0;
  }
  if(*last==1){
    *last=*last-1;
  }
  if(*last>1){
    *last=*last-1;
    queue[j]=queue[*last];
  }
 
  memcpy(queue[*last].full_packet, empty, MAX_PKT_LENGTH);
  queue[*last].port1 = 0;
  queue[*last].port2 = 0;
  queue[*last].port3 = 0;
  queue[*last].data_size = 0;
  queue[*last].timestamp.tv_sec = 0;
  queue[*last].timestamp.tv_nsec = 0;
  /*
  printf("AFTER cleaning:\n");
  for(i=0;i<PACKET_CACHE;i++){
    if(i==j){printf("X");}else{printf(" ");}
    printf("[%d]", i);
    for(x=0;x<20;x++){printf("%d ", queue[i].full_packet[x]);}
    printf(" | p1: %d p2: %d p3: %d", queue[i].port1, queue[i].port2, queue[i].port3);
    printf(" | clk: %ld\n", clock() - queue[i].timestamp);
  }
  */
  /*
  if(*last != 0){
    queue[j]=queue[*last];
    *last = *last-1;
  }else{
    memcpy(queue[j].full_packet, empty, MAX_PKT_LENGTH);
    queue[j].port1 = 0;
    queue[j].port2 = 0;
    queue[j].port3 = 0;
    queue[j].data_size = 0;
    queue[j].timestamp = 0;
  }
  */
/*  int x;
    do{
      memcpy(queue[j].full_packet, queue[j+1].full_packet, data_size);
      queue[j].port1 = queue[j+1].port1;
      queue[j].port2 = queue[j+1].port2;
      queue[j].port3 = queue[j+1].port3;
      queue[j].data_size = queue[j+1].data_size;
      queue[j].timestamp = queue[j+1].timestamp;
      j++;
    }while(memcmp(queue[j+1].full_packet, empty, MAX_PKT_LENGTH));
    memcpy(queue[j].full_packet, empty, MAX_PKT_LENGTH);
    queue[j].port1 = 0;
    queue[j].port2 = 0;
    queue[j].port3 = 0;
    queue[j].data_size = 0;
    queue[j].timestamp = 0;
    */
  return 0;
}

int main(){
  
    int i;						// counters for loops
//     int amount_send = 0;				// the number of bytes send out throu a socket
//     int data_size = 0;					// number of bytes current packet
    
//     int thread_error;					// return value of thread_create
    
//     unsigned char buffer[MAX_PKT_LENGTH];
    clock_t start_timer, stop_timer;
    memset(empty, 0, MAX_PKT_LENGTH);
    
    pthread_mutex_init(&printf_mutex, NULL);
    pthread_mutex_init(&cache0_mutex, NULL);
    pthread_mutex_init(&cache1_mutex, NULL);
    pthread_mutex_init(&cache2_mutex, NULL);
    pthread_mutex_init(&cache3_mutex, NULL);
    pthread_mutex_init(&cache4_mutex, NULL);
    pthread_mutex_init(&cache5_mutex, NULL);
    
    // receive buffers 
    /*
    struct pkt_struct * cache0 = (struct pkt_struct *)malloc(PACKET_CACHE * sizeof(struct pkt_struct));
    struct pkt_struct * cache1 = (struct pkt_struct *)malloc(PACKET_CACHE * sizeof(struct pkt_struct));
    struct pkt_struct * cache2 = (struct pkt_struct *)malloc(PACKET_CACHE * sizeof(struct pkt_struct));
    struct pkt_struct * cache3 = (struct pkt_struct *)malloc(PACKET_CACHE * sizeof(struct pkt_struct));
    struct pkt_struct * cache4 = (struct pkt_struct *)malloc(PACKET_CACHE * sizeof(struct pkt_struct));
    struct pkt_struct * cache5 = (struct pkt_struct *)malloc(PACKET_CACHE * sizeof(struct pkt_struct));
    */
    struct pkt_struct cache0[PACKET_CACHE];
    struct pkt_struct cache1[PACKET_CACHE];
    struct pkt_struct cache2[PACKET_CACHE];
    struct pkt_struct cache3[PACKET_CACHE];
    struct pkt_struct cache4[PACKET_CACHE];
    struct pkt_struct cache5[PACKET_CACHE];
    // collect queues
    //struct candidate * queue_left = (struct candidate *)malloc(PACKET_CACHE * sizeof(struct candidate));
    //struct candidate * queue_right = (struct candidate *)malloc(PACKET_CACHE * sizeof(struct candidate));
    struct candidate queue_left[PACKET_CACHE];
    struct candidate queue_right[PACKET_CACHE];
    
    struct candidate clear_candidate;
    memset(clear_candidate.full_packet, 0, MAX_PKT_LENGTH); 
    clear_candidate.port1 = 0;
    clear_candidate.port2 = 0;
    clear_candidate.port3 = 0;
    clear_candidate.data_size = 0;
    clear_candidate.timestamp.tv_sec = 0;
    clear_candidate.timestamp.tv_nsec = 0;
    
    start_timer = clock();
    for(i=0;i<PACKET_CACHE-1;i++){
      queue_left[i] = clear_candidate;
      queue_right[i] = clear_candidate;
    }
    stop_timer = clock() - start_timer;
    
    struct recv_thread_input recv_thread_input_0, recv_thread_input_1, recv_thread_input_2, recv_thread_input_3, recv_thread_input_4, recv_thread_input_5;
    
    printf("\nSetting up socket threads");
    
    pthread_t sock_thread0, sock_thread1, sock_thread2, sock_thread3, sock_thread4, sock_thread5;	// reception
    pthread_t collect_left, collect_right;
    
    sock_raw_0 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_0 < 0){
        perror("Socket creation error on eth0");
        return 1;
    }printf("\nSocket IDs: %d, ", sock_raw_0);
    
    sock_raw_1 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_1 < 0){
        perror("Socket creation error on eth1");
        return 1;
    }printf("%d, ", sock_raw_1);
    
    sock_raw_2 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_2 < 0){
        perror("Socket creation error on eth2");
        return 1;
    }printf("%d, ", sock_raw_2);
    
    sock_raw_3 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_3 < 0){
        perror("Socket creation error on eth3");
        return 1;
    }printf("%d, ", sock_raw_3);

    sock_raw_4 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_4 < 0){
        perror("Socket creation error on eth4");
        return 1;
    }printf("%d, ", sock_raw_4);
    
    sock_raw_5 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_5 < 0){
        perror("Socket creation error on eth5");
        return 1;
    }printf("%d, ", sock_raw_5);
    
    sock_raw_6 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_6 < 0){
        perror("Socket creation error on eth6");
        return 1;
    }printf("%d, ", sock_raw_6);
    
    sock_raw_7 = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
    if(sock_raw_7 < 0){
        perror("Socket creation error on eth7");
        return 1;
    }printf("%d", sock_raw_7);
    
    struct sockaddr_ll socket_address;
    socket_address.sll_family   = AF_PACKET;
/*  socket_address.sll_protocol = htons(ETH_P_ALL);
 * TODO:
 *	would this be the prefered option? Have to find out, but at current state (33.c) it produces
 * 		* random rearrivals of single packets, which get written into the candidate_cache and remain there as stale packet
 * 			-> candidate_cache fills up unnecessarily
 * 		* ~0.5% packet loss
 */   
    socket_address.sll_protocol = htons(ETH_P_IP);
//     socket_address.sll_ifindex  = 0; 				// find interface index by running ./OT script - TODO: Integrate here
    socket_address.sll_hatype   = ARPHRD_ETHER;
    socket_address.sll_pkttype  = PACKET_OTHERHOST;
    socket_address.sll_halen    = ETH_ALEN;			// needed otherwise sendto fails
    
    //     h3-eth0
    struct sockaddr_ll socket_address0;
    socket_address0 = socket_address;
//     socket_address0.sll_ifindex = 2;
    socket_address0.sll_ifindex = find_ifIndex("h3-eth0");
    if (bind(sock_raw_0, (struct sockaddr*) &socket_address0, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_0);
      return 1;
    }
    //     h3-eth1
    struct sockaddr_ll socket_address1;
    socket_address1 = socket_address;
//     socket_address1.sll_ifindex = 3;
    socket_address1.sll_ifindex = find_ifIndex("h3-eth1");
    if (bind(sock_raw_1, (struct sockaddr*) &socket_address1, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_1);
      return 1;
    }
    //     h3-eth2
    struct sockaddr_ll socket_address2;
    socket_address2 = socket_address;
//     socket_address2.sll_ifindex = 4;
    socket_address2.sll_ifindex = find_ifIndex("h3-eth2");
    if (bind(sock_raw_2, (struct sockaddr*) &socket_address2, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_2);
      return 1;
    }
    //     h3-eth3
    struct sockaddr_ll socket_address3;
    socket_address3 = socket_address;
//     socket_address3.sll_ifindex = 5;
    socket_address3.sll_ifindex = find_ifIndex("h3-eth3");
    if (bind(sock_raw_3, (struct sockaddr*) &socket_address3, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_3);
      return 1;
    }
    //     h3-eth4
    struct sockaddr_ll socket_address4;
    socket_address4 = socket_address;
//     socket_address4.sll_ifindex = 6;
    socket_address4.sll_ifindex = find_ifIndex("h3-eth4");
    if (bind(sock_raw_4, (struct sockaddr*) &socket_address4, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_4);
      return 1;
    }
    //     h3-eth5
    struct sockaddr_ll socket_address5;
    socket_address5 = socket_address;
//     socket_address5.sll_ifindex = 7;
    socket_address5.sll_ifindex = find_ifIndex("h3-eth5");
    if (bind(sock_raw_5, (struct sockaddr*) &socket_address5, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_5);
      return 1;
    }
        //     h3-eth6
    struct sockaddr_ll socket_address6;
    socket_address6 = socket_address;
//     socket_address5.sll_ifindex = 8;
    socket_address6.sll_ifindex = find_ifIndex("h3-eth6");
    if (bind(sock_raw_6, (struct sockaddr*) &socket_address6, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_6);
      return 1;
    }
        //     h3-eth7
    struct sockaddr_ll socket_address7;
    socket_address7 = socket_address;
//     socket_address5.sll_ifindex = 9;
    socket_address7.sll_ifindex = find_ifIndex("h3-eth7");
    if (bind(sock_raw_7, (struct sockaddr*) &socket_address7, sizeof(socket_address)) < 0) {
      perror("bind failed\n");
      close(sock_raw_7);
      return 1;
    }
    

    // setting up receive thread inputs and creating threads
    recv_thread_input_0.sock_id = sock_raw_0;
    recv_thread_input_0.port = "h3-eth0";
    recv_thread_input_0.cache_start = cache0;
    recv_thread_input_0.cache_mutex = cache0_mutex;
    pthread_create(&sock_thread0, NULL, receive_setup, &recv_thread_input_0);
    sleep(1);
    
    recv_thread_input_1.sock_id = sock_raw_1;
    recv_thread_input_1.port = "h3-eth1";
    recv_thread_input_1.cache_start = cache1;
    recv_thread_input_1.cache_mutex = cache1_mutex;
    pthread_create(&sock_thread1, NULL, receive_setup, &recv_thread_input_1);
    sleep(1);
    
    recv_thread_input_2.sock_id = sock_raw_2;
    recv_thread_input_2.port = "h3-eth2";
    recv_thread_input_2.cache_start = cache2;
    recv_thread_input_2.cache_mutex = cache2_mutex;
    pthread_create(&sock_thread2, NULL, receive_setup, &recv_thread_input_2);
    sleep(1);
    
    recv_thread_input_3.sock_id = sock_raw_3;
    recv_thread_input_3.port = "h3-eth3";
    recv_thread_input_3.cache_start = cache3;
    recv_thread_input_3.cache_mutex = cache3_mutex;
    pthread_create(&sock_thread3, NULL, receive_setup, &recv_thread_input_3);
    sleep(1);
    
    recv_thread_input_4.sock_id = sock_raw_4;
    recv_thread_input_4.port = "h3-eth4";
    recv_thread_input_4.cache_start = cache4;
    recv_thread_input_4.cache_mutex = cache4_mutex;
    pthread_create(&sock_thread4, NULL, receive_setup, &recv_thread_input_4);
    sleep(1);
    
    recv_thread_input_5.sock_id = sock_raw_5;
    recv_thread_input_5.port = "h3-eth5";
    recv_thread_input_5.cache_start = cache5;
    recv_thread_input_5.cache_mutex = cache5_mutex;
    pthread_create(&sock_thread5, NULL, receive_setup, &recv_thread_input_5);
    sleep(1);
    
    
    // setting up and creating collect and search threads
    struct collect_thread_input collect_thread_input_left;
      collect_thread_input_left.left = 1;
      collect_thread_input_left.candidate_queue = queue_left;
      collect_thread_input_left.cache1 = &recv_thread_input_0;
      collect_thread_input_left.cache2 = &recv_thread_input_1;
      collect_thread_input_left.cache3 = &recv_thread_input_2;
      collect_thread_input_left.socket_address = socket_address6;
      collect_thread_input_left.send_sock_id = sock_raw_6;
    
    struct collect_thread_input collect_thread_input_right;
      collect_thread_input_right.left = 0;
      collect_thread_input_right.candidate_queue = queue_right;
      collect_thread_input_right.cache1 = &recv_thread_input_3;
      collect_thread_input_right.cache2 = &recv_thread_input_4;
      collect_thread_input_right.cache3 = &recv_thread_input_5;
      collect_thread_input_right.socket_address = socket_address7;
      collect_thread_input_right.send_sock_id = sock_raw_7;
      
    pthread_create(&collect_left, NULL, collect_and_send, &collect_thread_input_left);
    pthread_create(&collect_right, NULL, collect_and_send, &collect_thread_input_right);
    
    printf("\nStarting......PID : %d.......Cleaning took %ld clks..................................................", getpid(), stop_timer);
    printf("\nSetup done, \n Press Ctrl c to exit...");
    
    // register the signal handler, to exit the loop
    signal(SIGINT, sig_handler);
    
    pthread_join(sock_thread0, NULL);
    pthread_join(sock_thread1, NULL);
    pthread_join(sock_thread2, NULL);
    pthread_join(sock_thread3, NULL);
    pthread_join(sock_thread4, NULL);
    pthread_join(sock_thread5, NULL);
    
    pthread_join(collect_left, NULL);
    pthread_join(collect_right, NULL);
    
    close(sock_raw_0);
    close(sock_raw_1);
    close(sock_raw_2);
    close(sock_raw_3);
    close(sock_raw_4);
    close(sock_raw_5);
    /*
    free(cache0);
    free(cache1);
    free(cache2);
    free(cache3);
    free(cache4);
    free(cache5);
    */
//     free(queue_left);
//     free(queue_right);
    
    printf("\nFinished\n");
    return 0;
} 
