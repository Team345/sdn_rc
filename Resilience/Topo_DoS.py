# !/usr/bin/python
# topology used for DoS testing,
# resembles RC topology from performance tests but:
#  	candidates are switches (3), packet flood is generated on extra host h66
# each test switch port has a mirror port in RC. + 2 egress ports
# -> h3 has 8 interfaces

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from time import sleep

def myNetwork():

    net = Mininet( topo=None, build=False)

    info( '*** Add switches\n')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch)
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch)
    s5 = net.addSwitch('s5', cls=OVSKernelSwitch)
    s6 = net.addSwitch('s6', cls=OVSKernelSwitch)
    s7 = net.addSwitch('s7', cls=OVSKernelSwitch)
    s8 = net.addSwitch('s8', cls=OVSKernelSwitch)
    
    #switches substitute routers
    s13 = net.addSwitch('s13', cls=OVSKernelSwitch)
    s14 = net.addSwitch('s14', cls=OVSKernelSwitch)
    s15 = net.addSwitch('s15', cls=OVSKernelSwitch)

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, defaultRoute=None)
    h2 = net.addHost('h2', cls=Host, defaultRoute=None)
    
    h3 = net.addHost('h3', cls=Node)
    
    h66 = net.addHost('h66', cls=Node)

    info( '*** Add links\n')
    net.addLink(h1, s1)
    net.addLink(s1, s13)
    net.addLink(s1, s14)
    net.addLink(s1, s15)
    net.addLink(s1, s3)
    net.addLink(s1, s4)
    net.addLink(s1, s5)
    
    net.addLink(h2, s2)
    net.addLink(s2, s13)
    net.addLink(s2, s14)
    net.addLink(s2, s15)
    net.addLink(s2, s6)
    net.addLink(s2, s7)
    net.addLink(s2, s8)
    
    net.addLink(s3, h3)
    net.addLink(s4, h3)
    net.addLink(s5, h3)
    net.addLink(s6, h3)
    net.addLink(s7, h3)
    net.addLink(s8, h3)
    
    net.addLink(s1, h3)
    net.addLink(s2, h3)
    
    net.addLink(s13, h66)

    info( '*** Starting network\n')
    net.build()
 
    info( '*** Setting MAC and IP\n')
    # all one subnet
    h1 = net.get( 'h1' )
    h1.intf( 'h1-eth0' ).setMAC( '00:0C:00:00:00:01' )
    h1.intf( 'h1-eth0' ).setIP( '10.10.0.1', 24 )

    h2 = net.get( 'h2' )
    h2.intf( 'h2-eth0' ).setMAC( '00:05:00:00:00:01' )
    h2.intf( 'h2-eth0' ).setIP( '10.10.0.2', 24 )
    # RC
    h3 = net.get( 'h3' )
    h3.intf( 'h3-eth0' ).setMAC( '00:FF:00:00:00:01' )
    h3.intf( 'h3-eth0' ).setIP( '10.10.0.22', 24 )
    h3.intf( 'h3-eth1' ).setMAC( '00:FF:00:00:00:02' )
    h3.intf( 'h3-eth1' ).setIP( '10.10.0.33', 24 )
    h3.intf( 'h3-eth2' ).setMAC( '00:FF:00:00:00:03' )
    h3.intf( 'h3-eth2' ).setIP( '10.10.0.44', 24 )
    h3.intf( 'h3-eth3' ).setMAC( '00:FF:00:00:00:04' )
    h3.intf( 'h3-eth3' ).setIP( '10.10.0.55', 24 )
    h3.intf( 'h3-eth4' ).setMAC( '00:FF:00:00:00:05' )
    h3.intf( 'h3-eth4' ).setIP( '10.10.0.66', 24 )
    h3.intf( 'h3-eth5' ).setMAC( '00:FF:00:00:00:06' )
    h3.intf( 'h3-eth5' ).setIP( '10.10.0.77', 24 )
    
    h3.intf( 'h3-eth6' ).setMAC( '00:FF:00:00:00:07' )
    h3.intf( 'h3-eth6' ).setIP( '10.10.0.88', 24 )
    h3.intf( 'h3-eth7' ).setMAC( '00:FF:00:00:00:08' )
    h3.intf( 'h3-eth7' ).setIP( '10.10.0.99', 24 )

    h66 = net.get( 'h66' )
    h66.intf( 'h66-eth0' ).setMAC( '06:06:06:06:06:06' )
    h66.intf( 'h66-eth0' ).setIP( '10.10.0.66', 24 )

    # Add arp cache entries for hosts
    h1.cmd( 'arp -s 10.10.0.2 00:05:00:00:00:01 -i h1-eth0' )
    #h1.cmd( 'arp -s 10.10.0.2 00:05:00:00:00:01 -i h1-eth0' )
    h1.cmd( 'ethtool -K h1-eth0 tso off' )

    h2.cmd( 'arp -s 10.10.0.1 00:0C:00:00:00:01 -i h2-eth0' )
    #h2.cmd( 'arp -s 10.10.0.1 00:05:00:00:00:01 -i h2-eth0' )
    h2.cmd( 'ethtool -K h2-eth0 tso off' )
    
    h66.cmd( 'arp -s 10.10.0.2 00:05:00:00:00:01 -i h66-eth0' )
    h66.cmd( 'ethtool -K h66-eth0 tso off' )
    
    h3.cmd( 'ethtool -K h3-eth0 tso off' )
    h3.cmd( 'ethtool -K h3-eth1 tso off' )
    h3.cmd( 'ethtool -K h3-eth2 tso off' )
    h3.cmd( 'ethtool -K h3-eth3 tso off' )
    h3.cmd( 'ethtool -K h3-eth4 tso off' )
    h3.cmd( 'ethtool -K h3-eth5 tso off' )
    h3.cmd( 'ethtool -K h3-eth6 tso off' )
    h3.cmd( 'ethtool -K h3-eth7 tso off' )

    info( '*** Starting switches\n')
    net.get('s1').start([])
    net.get('s2').start([])
    net.get('s3').start([])
    net.get('s4').start([])
    net.get('s5').start([])
    net.get('s6').start([])
    net.get('s7').start([])
    net.get('s8').start([])
    
    net.get('s13').start([])
    net.get('s14').start([])
    net.get('s15').start([])

    # Add flow entries for switches
    s1 = net.get( 's1' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=1,actions=output:2,output:3,output:4' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=2,actions=output:5' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=3,actions=output:6' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=4,actions=output:7' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=8,actions=output:1' )
    
    s3 = net.get( 's3' )
    s3.cmd( 'ovs-ofctl add-flow s3 in_port=1,actions=output:2' )
    s3.cmd( 'ovs-ofctl add-flow s3 in_port=2,actions=output:1' )
    
    s4 = net.get( 's4' )
    s4.cmd( 'ovs-ofctl add-flow s4 in_port=1,actions=output:2' )
    s4.cmd( 'ovs-ofctl add-flow s4 in_port=2,actions=output:1' )
    
    s5 = net.get( 's5' )
    s5.cmd( 'ovs-ofctl add-flow s5 in_port=1,actions=output:2' )
    s5.cmd( 'ovs-ofctl add-flow s5 in_port=2,actions=output:1' )
    
    
    s2 = net.get( 's2' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=1,actions=output:2,output:3,output:4' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=2,actions=output:5' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=3,actions=output:6' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=4,actions=output:7' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=8,actions=output:1' )
    
    s6 = net.get( 's6' )
    s6.cmd( 'ovs-ofctl add-flow s6 in_port=1,actions=output:2' )
    s6.cmd( 'ovs-ofctl add-flow s6 in_port=2,actions=output:1' )
    
    s7 = net.get( 's7' )
    s7.cmd( 'ovs-ofctl add-flow s7 in_port=1,actions=output:2' )
    s7.cmd( 'ovs-ofctl add-flow s7 in_port=2,actions=output:1' )
    
    s8 = net.get( 's8' )
    s8.cmd( 'ovs-ofctl add-flow s8 in_port=1,actions=output:2' )
    s8.cmd( 'ovs-ofctl add-flow s8 in_port=2,actions=output:1' )
    
    s13 = net.get( 's13' )
    s13.cmd( 'ovs-ofctl add-flow s13 in_port=1,actions=output:2' )
    s13.cmd( 'ovs-ofctl add-flow s13 in_port=2,actions=output:1' )
    s13.cmd( 'ovs-ofctl add-flow s13 in_port=3,actions=output:2' )
    
    s14 = net.get( 's14' )
    s14.cmd( 'ovs-ofctl add-flow s14 in_port=1,actions=output:2' )
    s14.cmd( 'ovs-ofctl add-flow s14 in_port=2,actions=output:1' )
    
    s15 = net.get( 's15' )
    s15.cmd( 'ovs-ofctl add-flow s15 in_port=1,actions=output:2' )
    s15.cmd( 'ovs-ofctl add-flow s15 in_port=2,actions=output:1' )


    #start spamming on r3:
    #r3.cmd( 'nping --udp -p 80 --data-length 100 --delay 0ms -c 10 10.10.0.4 >> log_r3.txt' )
    #s2.cmd( 'sudp tcpdump -i s2-eth1 -U -w log_s2.txt ' )
    #s7.cmp( 'ovs-ofctl del-flows s7 "dl_dst=00:0C:00:00:00:06"' )
    #add threat flow:
    #s7.cmd( 'ovs-ofctl add-flow s11 dl_dst=00:0C:00:00:00:06,actions=output:1,mod_ip=172.10.0.1,output=3' )
    #h6.cmd( 'sudo tcpdump -i h6-eth0 ether dst 00:0C:00:00:00:06 -U -w log_h6.txt' )
    #h6.cmd( 'sleep 5' )
    
    #h6.cmd( 'tcpdump -i h6-eth0 ether dst 00:0C:00:00:00:06 -w log_h6.txt -U &' )
    #sleep(5)
    
    #h7.cmd( 'ping -c40 -i 0.1 h6' )
    #sleep(5)
    #h6.cmd( 'sudo pkill tcpdump' )
    #sleep(5)
    #h6.cmd( 'sudo kill $!' )
    #h6.cmd( 'pid=$(ps -e | pgrep tcpdump)' )
    #h6.cmd( 'kill -2 $pid' )
    #info( '\n:)\n' )
    
    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()