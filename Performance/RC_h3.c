/* Voter for tripple model redundancy scheme
 * tasks: 	- packet reception (raw socket)
 * 		- compares packets received on different interfaces and
 * 		 	sends packet if received on majority of interfaces
 * 		- cleans packets from buffer based on sending or timeout
 * 		- multithreaded ingress buffers
 * to be executed on h3, when running Topo_RC1.py
 * Author: Philipp Heyder
 * This version is part of my masters thesis, handed in on 11/3/2016
 */

/* to get PID */
#include <unistd.h>
#include<errno.h>
#include<stdio.h>
/* contains memcmp() */
#include<string.h>
/* defines htons */
#include <arpa/inet.h>
/* defines ETH_P_ALL */
#include<netinet/if_ether.h>
#include<sys/socket.h>
#include<sys/types.h>
/* contains socket_address.sll_pkttype */
#include<linux/if_packet.h>
#include<signal.h>
#include<pthread.h>
/* needed for find_ifIndex() */
#include<sys/ioctl.h>
/* also for find_ifIndex(), supplies struct ifr*/
#include<net/if.h>

/* upper bound for packet size */
#define MAX_PKT_LENGTH 1600	// 
/* upper bound for number of packets cached 
 *in reception and candidate buffers*/
#define PACKET_CACHE 100
/* number of votes needed for majority decission */
#define MAJORITY 2


/*Globals********************************************************************/
/* as long as loop == 1, 
 infinite loops for reception and majority decission continue */
int loop = 1;
/* packets remaining in candidate_queue for more than 100ms will be deleted */
unsigned long long TIMEOUT = 100000000;
/* socket identifiers */
int sock_raw_0, sock_raw_1, sock_raw_2, sock_raw_3, sock_raw_4,
    sock_raw_5, sock_raw_6, sock_raw_7;
/* zero string, to check for empty cache field compare*/
unsigned char empty[MAX_PKT_LENGTH];
/* a lock for printfs */
static pthread_mutex_t printf_mutex;
/* mutex locks for writing and searching for each cache */
static pthread_mutex_t cache0_mutex; 
static pthread_mutex_t cache1_mutex;
static pthread_mutex_t cache2_mutex;
static pthread_mutex_t cache3_mutex;
static pthread_mutex_t cache4_mutex;
static pthread_mutex_t cache5_mutex;

// socket setup recvfrom()
/* needed to declare sendto */
struct sockaddr saddr; 
int saddr_size = sizeof(saddr);

/* received packets + metadata */
struct pkt_struct
{
  unsigned char full_packet[MAX_PKT_LENGTH];
  int data_size;
  int timestamp;
};

/* all external inputs for packet reception */
struct recv_thread_input
{
  int sock_id;
  /* offset to next empty element */
  int head;
  /* offset to next element to move to */
  int tail;
  char *port;
  struct pkt_struct * cache_start;
  pthread_mutex_t cache_mutex;
};

/* packets and metadata needed for majority decission */
struct candidate
{
  unsigned char full_packet[MAX_PKT_LENGTH];
  unsigned char port1;
  unsigned char port2;
  unsigned char port3;
  int data_size;
  struct timespec timestamp;
};

/* all external inputs needed for majority decission */
struct collect_thread_input
{
  /* helper to format logging */
  char left;
  /* offset to first empty element, same as head in struct recv_thread_input*/
  int last_candidate;
  struct candidate * candidate_queue;
  struct recv_thread_input *cache1;
  struct recv_thread_input *cache2;
  struct recv_thread_input *cache3;
  struct sockaddr_ll socket_address;
  /* socket identifier of egress socket */
  int send_sock_id;
};

/*Prototypes*****************************************************************/
void sig_handler(int sig_num);

int find_ifIndex(char * if_name);

void *receive_setup(void *threadarg);

void *collect_and_send(void *threadarg);

int clean_Candidate(struct candidate * queue, int j, int * last,
		    int left, int data_size, int reason);

/*Function Declarations******************************************************/
/* sets loop == 0 on keyboard interrupt, starts voter shut down */
void sig_handler(int sig_num)
{
  if(sig_num == SIGINT)
  {
    printf("\n Caught the SIGINT signal\n Stopping and cleaning up...\n");
    shutdown(sock_raw_0, SHUT_RD);
    shutdown(sock_raw_1, SHUT_RD);
    shutdown(sock_raw_2, SHUT_RD);
    shutdown(sock_raw_3, SHUT_RD);
    shutdown(sock_raw_4, SHUT_RD);
    shutdown(sock_raw_5, SHUT_RD);
    loop = 0;
  }
  else
  {
    printf("\n Caught the signal number [%d]\n", sig_num);
  }
}

/* returns the interface index, input is interface name */
int find_ifIndex(char * if_name)
{
  struct ifreq ifr;
  /* create temporary socket */
  int fd = socket(AF_PACKET, SOCK_RAW,0);
  /* alert if creation failed */
  if (fd == -1) 
  {
    printf("Socket creation fail %s", strerror(errno));
  }
  size_t if_name_len = strlen(if_name);

  if (if_name_len<sizeof(ifr.ifr_name))
  {
    memcpy(ifr.ifr_name, if_name, if_name_len);
    ifr.ifr_name[if_name_len] = 0;
  }
  else
  {
    printf("interface name is too long");
  }
  
  /* find interface name to interface index maping and write index */
  if (ioctl(fd, SIOCGIFINDEX, &ifr) == -1)
  {
    printf("Error in ioctl %s", strerror(errno));
  }
  
  close(fd);
  return ifr.ifr_ifindex;
}

/* packet reception and handling ring buffer */
void *receive_setup(void *threadarg)
{
  /* expand thread input */
  struct recv_thread_input *reception = 
    (struct recv_thread_input *) threadarg;
  printf("\n---%s with Socket_ID: %d runs in Thread %lu cache addr: %p\n",
	reception->port, reception->sock_id, pthread_self(),
	reception->cache_start);
  
  reception->head = 0;
  reception->tail = 0;

  /* enter infinite reception loop */
  do{
    /* receive packet, write packet to reception buffer and save its size */
    reception->cache_start[reception->head].data_size = 
      recvfrom(reception->sock_id, 
	      reception->cache_start[reception->head].full_packet,
	      MAX_PKT_LENGTH , 0 , &saddr , (socklen_t*)&saddr_size);
    /* all MAC addresses in out topo start with 0,
	this prevents packets send by other Linux processes 
	(e.g., the network-manager) from entering the voter*/
    if(reception->cache_start[reception->head].full_packet[0] == 0){
      /* accomodates differing MAC addresses of candidate switches/routers, 
	  by rewriting the appropriateheader field */
      reception->cache_start[reception->head].full_packet[7] = 200;
      /* after packet is saved, move buffer head*/
      pthread_mutex_lock(&reception->cache_mutex);
      reception->head++;
      pthread_mutex_unlock(&reception->cache_mutex);
      /* check if buffer is full */
      if(reception->head >= PACKET_CACHE - 1)
      {
	/* wait until all packets have been read into candidate queue */
	while(reception->tail <= (reception->head - 1) )
	{};
	/* then wrap around and start from beginning */
	pthread_mutex_lock(&reception->cache_mutex);
	reception->head = 0;
	reception->tail = 0;
	pthread_mutex_unlock(&reception->cache_mutex);
      }
    }
    /* left here for debugging, printf "." if foreign packet received */
    else
    {
      /*printf("\n.");*/
    }
  }while(loop);

  return 0;
}

// collect packets from reception buffers by calling them round-robin style
void *collect_and_send(void *threadarg)
{
  /* expand thread input */
  struct collect_thread_input *collect = 
    (struct collect_thread_input *) threadarg;

  /* temporarily plug packet from reception buffer, save as temp_Packet[] */
  unsigned char * temp_Packet[MAX_PKT_LENGTH];
  collect->last_candidate = 0;
  /* after thread creation, print some management info */
  pthread_mutex_lock(&printf_mutex);
  if(collect->left == 0)
  {
    printf("\n***Right queue");
  }
  else
  {
    printf("***Left queue");
  }
    printf("\n***Collect Thread %lu watches: \n C1 %p\n C2 %p\n C3 %p", 
	   pthread_self(), 
	   collect->cache1->cache_start, 
	   collect->cache2->cache_start, 
	   collect->cache3->cache_start
	  );
    printf("\nwrites to: %p\n", collect->candidate_queue);
  pthread_mutex_unlock(&printf_mutex);

  /* call read_from_buffer on reception buffers in round-robin fashion */
  while(loop)
  {
    read_from_buffer(collect->cache1, collect->candidate_queue, collect->left, 
		     temp_Packet, 1, collect->send_sock_id,
		     collect->socket_address, &(collect->last_candidate)
		    );
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
    read_from_buffer(collect->cache2, collect->candidate_queue, collect->left,
		     temp_Packet, 2, collect->send_sock_id,
		     collect->socket_address, &(collect->last_candidate)
		    );
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
    read_from_buffer(collect->cache3, collect->candidate_queue, collect->left,
		     temp_Packet, 3, collect->send_sock_id,
		     collect->socket_address, &(collect->last_candidate)
		    );
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
  }
  return 0;
}

/* compares packets from reception buffer to each packet in candidate_queue */
int read_from_buffer(struct recv_thread_input * cache, 
		     struct candidate * candidate_queue, 
		     int left, 
		     unsigned char * temp_Packet, 
		     int which_cache, 
		     int send_sock_id, 
		     struct sockaddr_ll socket_address, 
		     int * last)
{
  /* counts through candidate_queue elements */
  int j;
  int data_size = 0;
  /* saves number of bytes sent on egress port */
  int amount_send;
  /* reason why clean_candidate() will be called*/
  int result;
  /* used to determine time out */
  unsigned long long duration;
  struct timespec end;

  /* voter algorithm starts here */
  if(cache->head < cache->tail)
  {
    printf("\n***tail overtook head: something is off***\nhead: %d tail %d\n", 
	   cache->head, cache->tail);
  }
  /* as long as there are elements in the reception buffer */
  while(cache->head > cache->tail && loop)
  {
    /* read buffer tail element into local temp_Packet and associated variables*/
    memset(temp_Packet, 0, MAX_PKT_LENGTH);
    data_size = cache->cache_start[cache->tail].data_size;
    memcpy(temp_Packet, 
	   cache->cache_start[cache->tail].full_packet, 
	   cache->cache_start[cache->tail].data_size);
    
    /* loop over candidate_queue */
    for(j=0; (j<=*last && loop); j++)
    {
      /* if candidate_queue is full -> clean it */
       if(*last==PACKET_CACHE-1)
       {
 	  j=0;
	  /* check every candidate_queue element for timeout */
	  while(j<*last){
	    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
	    duration = (end.tv_nsec - candidate_queue[j].timestamp.tv_nsec);
	    /* call clean_Candidate() for every timed out packet */
	    if(duration>TIMEOUT)
	    {
	      result = 3;
	      clean_Candidate(candidate_queue, j, last, left, data_size, result);
	    }
	    j++;
	  }
	  j=0;
	}
      /* candidate element is empty */
	if(j==*last)
	{
	/* note the reception port */
	  switch(which_cache)
	  {
	  case 1: candidate_queue[j].port1++;
		  if(candidate_queue[j].port1>1)
		  {
		    printf("\n!!Empty field has flooded ports: Port%d!!\n",
			 which_cache);
		   sleep(2);
		   return 0;
		  }
		  break;
	  case 2: candidate_queue[j].port2++;
		  if(candidate_queue[j].port2>1)
		  {
		    printf("\n!!Empty field has flooded ports: Port%d!!\n",
		 	 which_cache);
		    sleep(2);
		    return 0;
		  }
		  break;
	  case 3: candidate_queue[j].port3++;
		  if(candidate_queue[j].port3>1)
		  {
		    printf("\n!!Empty field has flooded ports: Port%d!!\n",
		 	 which_cache);
		    sleep(2);
		    return 0;
		  }
		  break;
	  default: printf("\n\n!!Trouble with read_from_buffer() input!!\n\n");
	  }
	  /* save the packet itself */
	  memcpy(candidate_queue[j].full_packet, temp_Packet, data_size);
	  /* timestamp the entry */
	  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &candidate_queue[j].timestamp);
	  *last = *last + 1;
	  pthread_mutex_lock(&cache->cache_mutex);
	  cache->tail++;
	  pthread_mutex_unlock(&cache->cache_mutex);
	  /* if all new elements were read -> end search */
	  if(cache->tail == cache->head){
	  return 0;
	}
	break;
      }
      /* temp_Packet matches a candidate element */
      if(!memcmp(temp_Packet, candidate_queue[j].full_packet, data_size))
      {
	/* note the reception port */
	switch(which_cache)
	{
	  case 1: candidate_queue[j].port1++;
		  /* alarm if packet was received on port more than once */
		  if(candidate_queue[j].port1>1 && cache->tail != 0 )
		  {
		    printf("\n!!Packet was already received on port%d!!\n",
			 which_cache);
		    sleep(2);
		    return 0;
		  }
		  break;
	  case 2: candidate_queue[j].port2++;
		  if(candidate_queue[j].port2>1 && cache->tail != 0)
		  {
		    printf("\n!!Packet was already received on port%d!!\n", 
			 which_cache);
		    sleep(2);
		    return 0;
		  }
		  break;
	  case 3: candidate_queue[j].port3++;
		  if(candidate_queue[j].port3>1 && cache->tail != 0)
		  {
		    printf("\n!!Packet was already received on port%d!!\n",
			 which_cache);
		    sleep(2);
		    return 0;
		  }
		  break;
	  default: printf("\n!!Trouble with read_from_buffer() input!!\n");
	}
	/* if all new elements were read -> end search */
	if(cache->head == cache->tail)
	{
	  return 0;
	}
	else
	{
	  pthread_mutex_lock(&cache->cache_mutex);
	  cache->tail++;
	  pthread_mutex_unlock(&cache->cache_mutex);
	}
	/* if this pacekt was received by at least MAJORITY ports -> send it */
	if(candidate_queue[j].port1 + candidate_queue[j].port2 + 
	    candidate_queue[j].port3 == MAJORITY)
	{
	  amount_send = sendto(send_sock_id, temp_Packet, data_size, 0,
			       (struct sockaddr*)&socket_address, 
			       sizeof(socket_address));
	  if (amount_send < 0)
	  {
	    perror("\nsendto failed");
	  }
	}
	  /* if packet was recieved by all 3 ports -> clean the element */
	else if(candidate_queue[j].port1 + candidate_queue[j].port2 + 
		  candidate_queue[j].port3 > MAJORITY)
	{
	    result = 0;
	    clean_Candidate(candidate_queue, j, last, left, data_size, result);
	    j--;
	}
	break;
      }
    }
  }
  return 0;
}

// called to remove single element from candidate_queue
int clean_Candidate(struct candidate * queue, int j, int * last,
		    int left, int data_size, int reason)
{
  /* function called even though candidate_queue is empty */
  if((*last==0) && reason !=3)
  {
    printf("\n\n!!!Error *last == 0 and clean_Candidate was called!!\n\n");
    sleep(5);
    return 0;
  }
  /* only one element saves in queue -> can be overwritten by next entry */
  if(*last==1)
  {
    *last=*last-1;
  }
  /* more than one element in queue, 
      -> swap selected element with newest element */
  if(*last>1)
  {
    *last=*last-1;
    queue[j]=queue[*last];
  }
  /* if this point is reached, last element was moved
      -> clean current last element */
  memcpy(queue[*last].full_packet, empty, MAX_PKT_LENGTH);
  queue[*last].port1 = 0;
  queue[*last].port2 = 0;
  queue[*last].port3 = 0;
  queue[*last].data_size = 0;
  queue[*last].timestamp.tv_sec = 0;
  queue[*last].timestamp.tv_nsec = 0;

  return 0;
}
/*Main***********************************************************************/
/* only responsible for declarations and thread setup */
int main()
{
  /* counter for number of loops */
  int i;
  memset(empty, 0, MAX_PKT_LENGTH);
    
  pthread_mutex_init(&printf_mutex, NULL);
  pthread_mutex_init(&cache0_mutex, NULL);
  pthread_mutex_init(&cache1_mutex, NULL);
  pthread_mutex_init(&cache2_mutex, NULL);
  pthread_mutex_init(&cache3_mutex, NULL);
  pthread_mutex_init(&cache4_mutex, NULL);
  pthread_mutex_init(&cache5_mutex, NULL);
    
  /* reception buffers */
  struct pkt_struct cache0[PACKET_CACHE];
  struct pkt_struct cache1[PACKET_CACHE];
  struct pkt_struct cache2[PACKET_CACHE];
  struct pkt_struct cache3[PACKET_CACHE];
  struct pkt_struct cache4[PACKET_CACHE];
  struct pkt_struct cache5[PACKET_CACHE];

  /* candidate queues aggregate packets for majority decission */
  struct candidate queue_left[PACKET_CACHE];
  struct candidate queue_right[PACKET_CACHE];

  /* make sure that candidate queue is empty before operating on it */
  struct candidate clear_candidate;
  memset(clear_candidate.full_packet, 0, MAX_PKT_LENGTH); 
  clear_candidate.port1 = 0;
  clear_candidate.port2 = 0;
  clear_candidate.port3 = 0;
  clear_candidate.data_size = 0;
  clear_candidate.timestamp.tv_sec = 0;
  clear_candidate.timestamp.tv_nsec = 0;
    
  for(i=0;i<PACKET_CACHE-1;i++){
      queue_left[i] = clear_candidate;
      queue_right[i] = clear_candidate;
  }
  
  struct recv_thread_input recv_thread_input_0, recv_thread_input_1;
  struct recv_thread_input recv_thread_input_2, recv_thread_input_3;
  struct recv_thread_input recv_thread_input_4, recv_thread_input_5;
    
  printf("\nSetting up socket threads");
    
  pthread_t sock_thread0, sock_thread1, sock_thread2;
  pthread_t sock_thread3, sock_thread4, sock_thread5;
  
  pthread_t collect_left, collect_right;
    
  sock_raw_0 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_0 < 0)
  {
    perror("Socket creation error on eth0");
    return 1;
  }
  printf("\nSocket IDs: %d, ", sock_raw_0);
    
  sock_raw_1 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_1 < 0)
  {
    perror("Socket creation error on eth1");
    return 1;
  }
  printf("%d, ", sock_raw_1);
    
  sock_raw_2 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_2 < 0)
  {
    perror("Socket creation error on eth2");
    return 1;
  }
  printf("%d, ", sock_raw_2);
    
  sock_raw_3 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_3 < 0)
  {
    perror("Socket creation error on eth3");
    return 1;
  }
  printf("%d, ", sock_raw_3);

  sock_raw_4 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_4 < 0)
  {
    perror("Socket creation error on eth4");
    return 1;
  }
  printf("%d, ", sock_raw_4);
    
  sock_raw_5 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_5 < 0)
  {
    perror("Socket creation error on eth5");
    return 1;
  }
  printf("%d, ", sock_raw_5);
    
  sock_raw_6 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_6 < 0)
  {
    perror("Socket creation error on eth6");
    return 1;
  }
  printf("%d, ", sock_raw_6);
    
  sock_raw_7 = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if(sock_raw_7 < 0)
  {
    perror("Socket creation error on eth7");
    return 1;
  }
  printf("%d", sock_raw_7);
    
  struct sockaddr_ll socket_address;
  socket_address.sll_family   = AF_PACKET;
  socket_address.sll_protocol = htons(ETH_P_IP);
  socket_address.sll_hatype   = ARPHRD_ETHER;
  socket_address.sll_pkttype  = PACKET_OTHERHOST;
  socket_address.sll_halen    = ETH_ALEN;
    
  /* h3-eth0 */
  struct sockaddr_ll socket_address0;
  socket_address0 = socket_address;
  socket_address0.sll_ifindex = find_ifIndex("h3-eth0");
  if (bind(sock_raw_0, (struct sockaddr*) &socket_address0,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_0);
    return 1;
  }
  
  /* h3-eth1 */
  struct sockaddr_ll socket_address1;
  socket_address1 = socket_address;
  socket_address1.sll_ifindex = find_ifIndex("h3-eth1");
  if (bind(sock_raw_1, (struct sockaddr*) &socket_address1,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_1);
    return 1;
  }

  /* h3-eth2 */
  struct sockaddr_ll socket_address2;
  socket_address2 = socket_address;
  socket_address2.sll_ifindex = find_ifIndex("h3-eth2");
  if (bind(sock_raw_2, (struct sockaddr*) &socket_address2,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_2);
    return 1;
  }
  
  /* h3-eth3 */
  struct sockaddr_ll socket_address3;
  socket_address3 = socket_address;
  socket_address3.sll_ifindex = find_ifIndex("h3-eth3");
  if (bind(sock_raw_3, (struct sockaddr*) &socket_address3,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_3);
    return 1;
  }
  
  /* h3-eth4 */
  struct sockaddr_ll socket_address4;
  socket_address4 = socket_address;
  socket_address4.sll_ifindex = find_ifIndex("h3-eth4");
  if (bind(sock_raw_4, (struct sockaddr*) &socket_address4,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_4);
    return 1;
  }
  
  /* h3-eth5 */
  struct sockaddr_ll socket_address5;
  socket_address5 = socket_address;
  socket_address5.sll_ifindex = find_ifIndex("h3-eth5");
  if (bind(sock_raw_5, (struct sockaddr*) &socket_address5,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_5);
    return 1;
  }
  
  /* h3-eth6 */
  struct sockaddr_ll socket_address6;
  socket_address6 = socket_address;
  socket_address6.sll_ifindex = find_ifIndex("h3-eth6");
  if (bind(sock_raw_6, (struct sockaddr*) &socket_address6,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_6);
    return 1;
  }
  
  /* h3-eth7 */
  struct sockaddr_ll socket_address7;
  socket_address7 = socket_address;
  socket_address7.sll_ifindex = find_ifIndex("h3-eth7");
  if (bind(sock_raw_7, (struct sockaddr*) &socket_address7,
    sizeof(socket_address)) < 0)
  {
    perror("bind failed\n");
    close(sock_raw_7);
    return 1;
  }
    

  /* setting up receive thread inputs and creating threads */
  recv_thread_input_0.sock_id = sock_raw_0;
  recv_thread_input_0.port = "h3-eth0";
  recv_thread_input_0.cache_start = cache0;
  recv_thread_input_0.cache_mutex = cache0_mutex;
  pthread_create(&sock_thread0, NULL, receive_setup, &recv_thread_input_0);
  sleep(1);
    
  recv_thread_input_1.sock_id = sock_raw_1;
  recv_thread_input_1.port = "h3-eth1";
  recv_thread_input_1.cache_start = cache1;
  recv_thread_input_1.cache_mutex = cache1_mutex;
  pthread_create(&sock_thread1, NULL, receive_setup, &recv_thread_input_1);
  sleep(1);
    
  recv_thread_input_2.sock_id = sock_raw_2;
  recv_thread_input_2.port = "h3-eth2";
  recv_thread_input_2.cache_start = cache2;
  recv_thread_input_2.cache_mutex = cache2_mutex;
  pthread_create(&sock_thread2, NULL, receive_setup, &recv_thread_input_2);
  sleep(1);
    
  recv_thread_input_3.sock_id = sock_raw_3;
  recv_thread_input_3.port = "h3-eth3";
  recv_thread_input_3.cache_start = cache3;
  recv_thread_input_3.cache_mutex = cache3_mutex;
  pthread_create(&sock_thread3, NULL, receive_setup, &recv_thread_input_3);
  sleep(1);
    
  recv_thread_input_4.sock_id = sock_raw_4;
  recv_thread_input_4.port = "h3-eth4";
  recv_thread_input_4.cache_start = cache4;
  recv_thread_input_4.cache_mutex = cache4_mutex;
  pthread_create(&sock_thread4, NULL, receive_setup, &recv_thread_input_4);
  sleep(1);
    
  recv_thread_input_5.sock_id = sock_raw_5;
  recv_thread_input_5.port = "h3-eth5";
  recv_thread_input_5.cache_start = cache5;
  recv_thread_input_5.cache_mutex = cache5_mutex;
  pthread_create(&sock_thread5, NULL, receive_setup, &recv_thread_input_5);
  sleep(1);
    
    
  /* setting up and creating collect and search threads */
  struct collect_thread_input collect_thread_input_left;
  collect_thread_input_left.left = 1;
  collect_thread_input_left.candidate_queue = queue_left;
  collect_thread_input_left.cache1 = &recv_thread_input_0;
  collect_thread_input_left.cache2 = &recv_thread_input_1;
  collect_thread_input_left.cache3 = &recv_thread_input_2;
  collect_thread_input_left.socket_address = socket_address6;
  collect_thread_input_left.send_sock_id = sock_raw_6;
    
  struct collect_thread_input collect_thread_input_right;
  collect_thread_input_right.left = 0;
  collect_thread_input_right.candidate_queue = queue_right;
  collect_thread_input_right.cache1 = &recv_thread_input_3;
  collect_thread_input_right.cache2 = &recv_thread_input_4;
  collect_thread_input_right.cache3 = &recv_thread_input_5;
  collect_thread_input_right.socket_address = socket_address7;
  collect_thread_input_right.send_sock_id = sock_raw_7;
      
  pthread_create(&collect_left, NULL, collect_and_send, 
		 &collect_thread_input_left);
  pthread_create(&collect_right, NULL, collect_and_send, 
		 &collect_thread_input_right);
    
  printf("\nStarting......PID :%d...................", getpid());
	 
  printf("\nSetup done, \n Press Ctrl c to exit...");
    
  /* register the signal handler, to exit the loop */
  signal(SIGINT, sig_handler);
  
  /* shut everything down */
  pthread_join(sock_thread0, NULL);
  pthread_join(sock_thread1, NULL);
  pthread_join(sock_thread2, NULL);
  pthread_join(sock_thread3, NULL);
  pthread_join(sock_thread4, NULL);
  pthread_join(sock_thread5, NULL);
    
  pthread_join(collect_left, NULL);
  pthread_join(collect_right, NULL);
    
  close(sock_raw_0);
  close(sock_raw_1);
  close(sock_raw_2);
  close(sock_raw_3);
  close(sock_raw_4);
  close(sock_raw_5);
    
  printf("\nFinished\n");
  return 0;
}