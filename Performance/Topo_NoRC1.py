#!/usr/bin/python
# Single router scenario, no combiner elements
# CLI starts after setup

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from time import sleep

def myNetwork():

    net = Mininet( topo=None, build=False)

    info( '*** Add routers\n')
    r3 = net.addHost('r3', cls=Node)
    r3.cmd('sysctl -w net.ipv4.ip_forward=1')

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, defaultRoute=None)
    h2 = net.addHost('h2', cls=Host, defaultRoute=None)

    info( '*** Add links\n')
    net.addLink(h1, r3)
    net.addLink(h2, r3)

    info( '*** Starting network\n')
    net.build()
 
    info( '*** Setting MAC and IP\n')
    h1 = net.get( 'h1' )
    h1.intf( 'h1-eth0' ).setMAC( '00:0C:00:00:00:01' )
    h1.intf( 'h1-eth0' ).setIP( '10.10.0.11', 24 )
    h2 = net.get( 'h2' )
    h2.intf( 'h2-eth0' ).setMAC( '00:05:00:00:00:01' )
    h2.intf( 'h2-eth0' ).setIP( '172.10.0.11', 24 )

    # Router 1
    r3 = net.get( 'r3' )
    r3.intf( 'r3-eth0' ).setMAC( '00:D1:00:00:00:01' )
    r3.intf( 'r3-eth0' ).setIP( '10.10.0.1', 24 )
    r3.intf( 'r3-eth1' ).setMAC( '00:D1:00:00:00:02' )
    r3.intf( 'r3-eth1' ).setIP( '172.10.0.1', 24 )
    
    # set gateways to route through r3
    h1.cmd( 'route add default gw 10.10.0.1 dev h1-eth0' )
    h2.cmd( 'route add default gw 172.10.0.1 dev h2-eth0' )

    # Add arp cache entries for hosts
    h1.cmd( 'arp -s 10.10.0.1 00:D1:00:00:00:01 -i h1-eth0' )
    h1.cmd( 'ethtool -K h1-eth0 tso off' )

    h2.cmd( 'arp -s 172.10.0.1 00:D1:00:00:00:02 -i h2-eth0' )
    h2.cmd( 'ethtool -K h2-eth0 tso off' )

    r3.cmd( 'arp -s 10.10.0.11 00:0C:00:00:00:01 -i r3-eth0' )
    r3.cmd( 'arp -s 172.10.0.11 00:05:00:00:00:01 -i r3-eth1' )
    
    CLI(net)    
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()