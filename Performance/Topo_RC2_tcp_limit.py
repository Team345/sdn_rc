#!/usr/bin/python
# Multi combiner topology, voter on h3 and h13
# preparation for muliple combiners -> limits cpu usage, then starts TCP tests


from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from time import sleep

def myNetwork():

    net = Mininet( topo=None, build=False)

    info( '*** Add switches\n')
      #left
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch)
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch)
    s5 = net.addSwitch('s5', cls=OVSKernelSwitch)
    s6 = net.addSwitch('s6', cls=OVSKernelSwitch)
    s7 = net.addSwitch('s7', cls=OVSKernelSwitch)
    s8 = net.addSwitch('s8', cls=OVSKernelSwitch)
      #right
    s11 = net.addSwitch('s11', cls=OVSKernelSwitch)
    s12 = net.addSwitch('s12', cls=OVSKernelSwitch)
    s13 = net.addSwitch('s13', cls=OVSKernelSwitch)
    s14 = net.addSwitch('s14', cls=OVSKernelSwitch)
    s15 = net.addSwitch('s15', cls=OVSKernelSwitch)
    s16 = net.addSwitch('s16', cls=OVSKernelSwitch)
    s17 = net.addSwitch('s17', cls=OVSKernelSwitch)
    s18 = net.addSwitch('s18', cls=OVSKernelSwitch)
      # replace r3, r4 and r5 on right side
    s83 = net.addSwitch('s83', cls=OVSKernelSwitch)
    s84 = net.addSwitch('s84', cls=OVSKernelSwitch)
    s85 = net.addSwitch('s85', cls=OVSKernelSwitch)
      
    info( '*** Add routers\n')
    r3 = net.addHost('r3', cls=Node)
    r3.cmd('sysctl -w net.ipv4.ip_forward=1')
    r4 = net.addHost('r4', cls=Node)
    r4.cmd('sysctl -w net.ipv4.ip_forward=1')
    r5 = net.addHost('r5', cls=Node)
    r5.cmd('sysctl -w net.ipv4.ip_forward=1')
    
    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, defaultRoute=None)
    h2 = net.addHost('h2', cls=Host, defaultRoute=None)
    h3 = net.addHost('h3', cls=Node) # first combiner
    h13 = net.addHost('h13', cls=Node) # second combiner
    
    info( '*** Add links\n')
      #left
    net.addLink(s1, h1)
    net.addLink(s1, r3)
    net.addLink(s1, r4)
    net.addLink(s1, r5)
    net.addLink(s1, s3)
    net.addLink(s1, s4)
    net.addLink(s1, s5)
    
    net.addLink(s2, s11)
    net.addLink(s2, r3)
    net.addLink(s2, r4)
    net.addLink(s2, r5)
    net.addLink(s2, s6)
    net.addLink(s2, s7)
    net.addLink(s2, s8)
    
    net.addLink(s3, h3)
    net.addLink(s4, h3)
    net.addLink(s5, h3)
    net.addLink(s6, h3)
    net.addLink(s7, h3)
    net.addLink(s8, h3)
    
    net.addLink(s1, h3)
    net.addLink(s2, h3)
      
      #right
    net.addLink(s11, s83)
    net.addLink(s11, s84)
    net.addLink(s11, s85)
    net.addLink(s11, s13)
    net.addLink(s11, s14)
    net.addLink(s11, s15)
    
    net.addLink(s12, h2)
    net.addLink(s12, s83)
    net.addLink(s12, s84)
    net.addLink(s12, s85)
    net.addLink(s12, s16)
    net.addLink(s12, s17)
    net.addLink(s12, s18)
    
    net.addLink(s13, h13)
    net.addLink(s14, h13)
    net.addLink(s15, h13)
    net.addLink(s16, h13)
    net.addLink(s17, h13)
    net.addLink(s18, h13)
    
    net.addLink(s11, h13)
    net.addLink(s12, h13)
    
    info( '*** Starting network\n')
    net.build()
 
    info( '*** Setting MAC and IP\n')
    # client 
    h1 = net.get( 'h1' )
    h1.intf( 'h1-eth0' ).setMAC( '00:0C:00:00:00:01' )
    h1.intf( 'h1-eth0' ).setIP( '10.10.0.11', 24 )
    # server
    h2 = net.get( 'h2' )
    h2.intf( 'h2-eth0' ).setMAC( '00:64:00:00:00:01' )	# needed to adjust this, so that received icmp probes are answered by h2 (0x64 = 16x100)
    h2.intf( 'h2-eth0' ).setIP( '172.10.0.11', 24 )
    # RC
    h3 = net.get( 'h3' )
    h3.intf( 'h3-eth0' ).setMAC( '00:FF:00:00:00:01' )
    h3.intf( 'h3-eth0' ).setIP( '10.10.0.22', 24 )
    h3.intf( 'h3-eth1' ).setMAC( '00:FF:00:00:00:02' )
    h3.intf( 'h3-eth1' ).setIP( '10.10.0.33', 24 )
    h3.intf( 'h3-eth2' ).setMAC( '00:FF:00:00:00:03' )
    h3.intf( 'h3-eth2' ).setIP( '10.10.0.44', 24 )
    h3.intf( 'h3-eth3' ).setMAC( '00:FF:00:00:00:04' )
    h3.intf( 'h3-eth3' ).setIP( '172.10.0.22', 24 )
    h3.intf( 'h3-eth4' ).setMAC( '00:FF:00:00:00:05' )
    h3.intf( 'h3-eth4' ).setIP( '172.10.0.33', 24 )
    h3.intf( 'h3-eth5' ).setMAC( '00:FF:00:00:00:06' )
    h3.intf( 'h3-eth5' ).setIP( '172.10.0.44', 24 )
    
    h3.intf( 'h3-eth6' ).setMAC( '00:FF:00:00:00:07' )
    h3.intf( 'h3-eth6' ).setIP( '10.10.0.55', 24 )
    h3.intf( 'h3-eth7' ).setMAC( '00:FF:00:00:00:08' )
    h3.intf( 'h3-eth7' ).setIP( '172.10.0.55', 24 )
    
    h13 = net.get( 'h13' )
    h13.intf( 'h13-eth0' ).setMAC( '00:C:00:00:00:01' )
    h13.intf( 'h13-eth0' ).setIP( '172.10.0.122', 24 )
    h13.intf( 'h13-eth1' ).setMAC( '00:AA:00:00:00:02' )
    h13.intf( 'h13-eth1' ).setIP( '172.10.0.133', 24 )
    h13.intf( 'h13-eth2' ).setMAC( '00:AA:00:00:00:03' )
    h13.intf( 'h13-eth2' ).setIP( '172.10.0.144', 24 )
    h13.intf( 'h13-eth3' ).setMAC( '00:AA:00:00:00:04' )
    h13.intf( 'h13-eth3' ).setIP( '172.10.0.22', 24 )
    h13.intf( 'h13-eth4' ).setMAC( '00:AA:00:00:00:05' )
    h13.intf( 'h13-eth4' ).setIP( '172.10.0.33', 24 )
    h13.intf( 'h13-eth5' ).setMAC( '00:AA:00:00:00:06' )
    h13.intf( 'h13-eth5' ).setIP( '172.10.0.44', 24 )
    
    h13.intf( 'h13-eth6' ).setMAC( '00:AA:00:00:00:07' )
    h13.intf( 'h13-eth6' ).setIP( '10.10.0.5', 24 )
    h13.intf( 'h13-eth7' ).setMAC( '00:AA:00:00:00:08' )
    h13.intf( 'h13-eth7' ).setIP( '172.10.0.5', 24 )

    	#left
    r3 = net.get( 'r3' )
    r3.intf( 'r3-eth0' ).setMAC( '00:D1:00:00:00:01' )
    r3.intf( 'r3-eth0' ).setIP( '10.10.0.1', 24 )
    r3.intf( 'r3-eth1' ).setMAC( '00:D1:00:00:00:02' )
    r3.intf( 'r3-eth1' ).setIP( '172.10.0.1', 24 )
    r4 = net.get( 'r4' )
    r4.intf( 'r4-eth0' ).setMAC( '00:D2:00:00:00:01' )
    r4.intf( 'r4-eth0' ).setIP( '10.10.0.2', 24 )
    r4.intf( 'r4-eth1' ).setMAC( '00:D2:00:00:00:02' )
    r4.intf( 'r4-eth1' ).setIP( '172.10.0.2', 24 )
    r5 = net.get( 'r5' )
    r5.intf( 'r5-eth0' ).setMAC( '00:D3:00:00:00:01' )
    r5.intf( 'r5-eth0' ).setIP( '10.10.0.3', 24 )
    r5.intf( 'r5-eth1' ).setMAC( '00:D3:00:00:00:02' )
    r5.intf( 'r5-eth1' ).setIP( '172.10.0.3', 24 )

    h1.cmd( 'route add default gw 10.10.0.1 dev h1-eth0' )
    h2.cmd( 'route add default gw 172.10.0.10 dev h2-eth0' )

    # Add arp cache entries for hosts
    h1.cmd( 'arp -s 10.10.0.1 00:D1:00:00:00:01 -i h1-eth0' )
    h1.cmd( 'arp -s 10.10.0.2 00:D2:00:00:00:01 -i h1-eth0' )
    h1.cmd( 'arp -s 10.10.0.3 00:D3:00:00:00:01 -i h1-eth0' )
    h1.cmd( 'ethtool -K h1-eth0 tso off' )

    h2.cmd( 'arp -s 172.10.0.10 00:D1:00:00:00:02 -i h2-eth0' )
    h2.cmd( 'arp -s 172.10.0.20 00:D2:00:00:00:02 -i h2-eth0' )
    h2.cmd( 'arp -s 172.10.0.30 00:D3:00:00:00:02 -i h2-eth0' )
    h2.cmd( 'ethtool -K h2-eth0 tso off' )
    
    h3.cmd( 'ethtool -K h3-eth0 tso off' )
    h3.cmd( 'ethtool -K h3-eth1 tso off' )
    h3.cmd( 'ethtool -K h3-eth2 tso off' )
    h3.cmd( 'ethtool -K h3-eth3 tso off' )
    h3.cmd( 'ethtool -K h3-eth4 tso off' )
    h3.cmd( 'ethtool -K h3-eth5 tso off' )
    h3.cmd( 'ethtool -K h3-eth6 tso off' )
    h3.cmd( 'ethtool -K h3-eth7 tso off' )
    
    h13.cmd( 'ethtool -K h13-eth0 tso off' )
    h13.cmd( 'ethtool -K h13-eth1 tso off' )
    h13.cmd( 'ethtool -K h13-eth2 tso off' )
    h13.cmd( 'ethtool -K h13-eth3 tso off' )
    h13.cmd( 'ethtool -K h13-eth4 tso off' )
    h13.cmd( 'ethtool -K h13-eth5 tso off' )
    h13.cmd( 'ethtool -K h13-eth6 tso off' )
    h13.cmd( 'ethtool -K h13-eth7 tso off' )
    
    r3.cmd( 'arp -s 10.10.0.11 00:0C:00:00:00:01 -i r3-eth0' )
    r3.cmd( 'arp -s 172.10.0.11 00:05:00:00:00:01 -i r3-eth1' )

    r4.cmd( 'arp -s 10.10.0.11 00:0C:00:00:00:01 -i r4-eth0' )
    r4.cmd( 'arp -s 172.10.0.11 00:05:00:00:00:01 -i r4-eth1' )

    r5.cmd( 'arp -s 10.10.0.11 00:0C:00:00:00:01 -i r5-eth0' )
    r5.cmd( 'arp -s 172.10.0.11 00:05:00:00:00:01 -i r5-eth1' )

    info( '*** Starting switches\n')
    net.get('s1').start([])
    net.get('s2').start([])
    net.get('s3').start([])
    net.get('s4').start([])
    net.get('s5').start([])
    net.get('s6').start([])
    net.get('s7').start([])
    net.get('s8').start([])
    
    net.get('s11').start([])
    net.get('s12').start([])
    net.get('s13').start([])
    net.get('s14').start([])
    net.get('s15').start([])
    net.get('s16').start([])
    net.get('s17').start([])
    net.get('s18').start([])

    net.get('s83').start([])
    net.get('s84').start([])
    net.get('s85').start([])

    # Add flow entries for switches--left
    s1 = net.get( 's1' )
    s1.cmd('ovs-ofctl add-flow s1 in_port=1,actions=mod_dl_dst:00:D1:00:00:00:01,output:2,mod_dl_dst:00:D2:00:00:00:01,output:3,mod_dl_dst:00:D3:00:00:00:01,output:4,')
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=2,actions=output:5' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=3,actions=output:6' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=4,actions=output:7' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=8,actions=mod_dl_dst:00:0C:00:00:00:01,output:1' )
    
    s3 = net.get( 's3' )
    s3.cmd( 'ovs-ofctl add-flow s3 in_port=1,actions=output:2' )
    s3.cmd( 'ovs-ofctl add-flow s3 in_port=2,actions=output:1' )
    
    s4 = net.get( 's4' )
    s4.cmd( 'ovs-ofctl add-flow s4 in_port=1,actions=output:2' )
    s4.cmd( 'ovs-ofctl add-flow s4 in_port=2,actions=output:1' )
    
    s5 = net.get( 's5' )
    s5.cmd( 'ovs-ofctl add-flow s5 in_port=1,actions=output:2' )
    s5.cmd( 'ovs-ofctl add-flow s5 in_port=2,actions=output:1' )
    
    s2 = net.get( 's2' )
    s2.cmd('ovs-ofctl add-flow s2 in_port=1,actions=mod_dl_dst:00:D1:00:00:00:02,output:2,mod_dl_dst:00:D2:00:00:00:02,output:3,mod_dl_dst:00:D3:00:00:00:02,output:4')
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=2,actions=output:5' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=3,actions=output:6' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=4,actions=output:7' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=8,actions=output:1' )

    s6 = net.get( 's6' )
    s6.cmd( 'ovs-ofctl add-flow s6 in_port=1,actions=output:2' )
    s6.cmd( 'ovs-ofctl add-flow s6 in_port=2,actions=output:1' )
    
    s7 = net.get( 's7' )
    s7.cmd( 'ovs-ofctl add-flow s7 in_port=1,actions=output:2' )
    s7.cmd( 'ovs-ofctl add-flow s7 in_port=2,actions=output:1' )
    
    s8 = net.get( 's8' )
    s8.cmd( 'ovs-ofctl add-flow s8 in_port=1,actions=output:2' )
    s8.cmd( 'ovs-ofctl add-flow s8 in_port=2,actions=output:1' )
    
     # Add flow entries for switches--right
    s11 = net.get( 's11' )
    s11.cmd('ovs-ofctl add-flow s11 in_port=1,actions=mod_dl_dst:00:D1:00:00:00:01,output:2,mod_dl_dst:00:D2:00:00:00:01,output:3,mod_dl_dst:00:D3:00:00:00:01,output:4,')
    s11.cmd( 'ovs-ofctl add-flow s11 in_port=2,actions=output:5' )
    s11.cmd( 'ovs-ofctl add-flow s11 in_port=3,actions=output:6' )
    s11.cmd( 'ovs-ofctl add-flow s11 in_port=4,actions=output:7' )
    s11.cmd( 'ovs-ofctl add-flow s11 in_port=8,actions=output:1' )
    
    s13 = net.get( 's13' )
    s13.cmd( 'ovs-ofctl add-flow s13 in_port=1,actions=output:2' )
    s13.cmd( 'ovs-ofctl add-flow s13 in_port=2,actions=output:1' )
    
    s14 = net.get( 's14' )
    s14.cmd( 'ovs-ofctl add-flow s14 in_port=1,actions=output:2' )
    s14.cmd( 'ovs-ofctl add-flow s14 in_port=2,actions=output:1' )
    
    s15 = net.get( 's15' )
    s15.cmd( 'ovs-ofctl add-flow s15 in_port=1,actions=output:2' )
    s15.cmd( 'ovs-ofctl add-flow s15 in_port=2,actions=output:1' )
    
    s83 = net.get( 's83' )
    s83.cmd( 'ovs-ofctl add-flow s83 in_port=2,actions=output:1' )
    s83.cmd( 'ovs-ofctl add-flow s83 in_port=1,actions=output:2' )
    
    s84 = net.get( 's84' )
    s84.cmd( 'ovs-ofctl add-flow s84 in_port=2,actions=output:1' )
    s84.cmd( 'ovs-ofctl add-flow s84 in_port=1,actions=output:2' )
    
    s85 = net.get( 's85' )
    s85.cmd( 'ovs-ofctl add-flow s85 in_port=2,actions=output:1' )
    s85.cmd( 'ovs-ofctl add-flow s85 in_port=1,actions=output:2' )
    
    s12 = net.get( 's12' )
    s12.cmd('ovs-ofctl add-flow s12 in_port=1,actions=mod_dl_dst:00:D1:00:00:00:02,output:2,mod_dl_dst:00:D2:00:00:00:02,output:3,mod_dl_dst:00:D3:00:00:00:02,output:4')
    s12.cmd( 'ovs-ofctl add-flow s12 in_port=2,actions=output:5' )
    s12.cmd( 'ovs-ofctl add-flow s12 in_port=3,actions=output:6' )
    s12.cmd( 'ovs-ofctl add-flow s12 in_port=4,actions=output:7' )
    s12.cmd( 'ovs-ofctl add-flow s12 in_port=8,actions=output:1' )
    
    s16 = net.get( 's16' )
    s16.cmd( 'ovs-ofctl add-flow s16 in_port=1,actions=output:2' )
    s16.cmd( 'ovs-ofctl add-flow s16 in_port=2,actions=output:1' )
    
    s17 = net.get( 's17' )
    s17.cmd( 'ovs-ofctl add-flow s17 in_port=1,actions=output:2' )
    s17.cmd( 'ovs-ofctl add-flow s17 in_port=2,actions=output:1' )
    
    s18 = net.get( 's18' )
    s18.cmd( 'ovs-ofctl add-flow s18 in_port=1,actions=output:2' )
    s18.cmd( 'ovs-ofctl add-flow s18 in_port=2,actions=output:1' )
    sleep(6)
    info( '*** Starting RC on h3 and h13\n')
    h3.cmd( 'sudo cpulimit ./RCh3 -l 80 &' )
    h13.cmd( 'sudo cpulimit ./RCh13 -l 80 &' )
    sleep(6)

    h2.cmd( 'iperf -s -d > Topo_RC2_tcp_limit_log &' )
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_log' )
    sleep(2)
    info('1\n')
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('2\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('3\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('4\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('5\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('6\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('7\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('8\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('9\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    info('10\n')
    sleep(2)
    h1.cmd( 'iperf -c 172.10.0.11 -d >> Topo_RC2_tcp_limit_logc' )
    sleep(2)
    #h3.cmd( 'killall ./RCh3' )
    #h3.cmd( 'killall ./RCh13' )
    info('\n:)\n')
    
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()