#!/usr/bin/python
# Topology resembles Topo_RC, but h3 is substituted with a switch
# purpose of this is to estimate delays of exta links
# CLI starts when ready

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from time import sleep

def myNetwork():

    net = Mininet( topo=None, build=False)
    info( '*** Add switches\n')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch)
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch)
    s5 = net.addSwitch('s5', cls=OVSKernelSwitch)

    info( '*** Add routers\n')
    r3 = net.addHost('r3', cls=Node)
    r3.cmd('sysctl -w net.ipv4.ip_forward=1')

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, defaultRoute=None)
    h2 = net.addHost('h2', cls=Host, defaultRoute=None)

    info( '*** Add links\n')
    net.addLink(h1, s1)
    net.addLink(s1, r3)
    net.addLink(r3, s2)
    net.addLink(s2, s3)
    net.addLink(s3, s4)
    net.addLink(s4, s5)
    net.addLink(s5, h2)

    info( '*** Starting network\n')
    net.build()
 
    info( '*** Setting MAC and IP\n')
    h1 = net.get( 'h1' )
    h1.intf( 'h1-eth0' ).setMAC( '00:0C:00:00:00:01' )
    h1.intf( 'h1-eth0' ).setIP( '10.10.0.11', 24 )
    h2 = net.get( 'h2' )
    h2.intf( 'h2-eth0' ).setMAC( '00:05:00:00:00:01' )
    h2.intf( 'h2-eth0' ).setIP( '172.10.0.11', 24 )

    # Router 1
    r3 = net.get( 'r3' )
    r3.intf( 'r3-eth0' ).setMAC( '00:D1:00:00:00:01' )
    r3.intf( 'r3-eth0' ).setIP( '10.10.0.1', 24 )
    r3.intf( 'r3-eth1' ).setMAC( '00:D1:00:00:00:02' )
    r3.intf( 'r3-eth1' ).setIP( '172.10.0.1', 24 )
    
    # set gateways to route through r3
    h1.cmd( 'route add default gw 10.10.0.1 dev h1-eth0' )
    h2.cmd( 'route add default gw 172.10.0.1 dev h2-eth0' )

    # Add arp cache entries for hosts
    h1.cmd( 'arp -s 10.10.0.1 00:D1:00:00:00:01 -i h1-eth0' )
    h1.cmd( 'ethtool -K h1-eth0 tso off' )

    h2.cmd( 'arp -s 172.10.0.1 00:D1:00:00:00:02 -i h2-eth0' )
    h2.cmd( 'ethtool -K h2-eth0 tso off' )

    r3.cmd( 'arp -s 10.10.0.11 00:0C:00:00:00:01 -i r3-eth0' )
    r3.cmd( 'arp -s 172.10.0.11 00:05:00:00:00:01 -i r3-eth1' )
    
    info( '*** Starting switches\n')
    net.get('s1').start([])
    net.get('s2').start([])
    net.get('s3').start([])
    net.get('s4').start([])
    net.get('s5').start([])
    #info( '*** Starting RC on h3\n')
    #h3.cmd( 'sudo ./RCh3 &' )
    #sleep(6)
    
    s1 = net.get( 's1' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=1,actions=output:2' )
    s1.cmd( 'ovs-ofctl add-flow s1 in_port=2,actions=output:1' )
    
    s2 = net.get( 's2' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=1,actions=output:2' )
    s2.cmd( 'ovs-ofctl add-flow s2 in_port=2,actions=output:1' )
    
    s3 = net.get( 's3' )
    s3.cmd( 'ovs-ofctl add-flow s3 in_port=1,actions=output:2' )
    s3.cmd( 'ovs-ofctl add-flow s3 in_port=2,actions=output:1' )
    
    s4 = net.get( 's4' )
    s4.cmd( 'ovs-ofctl add-flow s4 in_port=1,actions=output:2' )
    s4.cmd( 'ovs-ofctl add-flow s4 in_port=2,actions=output:1' )

    s5 = net.get( 's5' )
    s5.cmd( 'ovs-ofctl add-flow s5 in_port=1,actions=output:2' )
    s5.cmd( 'ovs-ofctl add-flow s5 in_port=2,actions=output:1' )
    
    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()